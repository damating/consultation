package controllers;

import models.Consultation;
import models.MailClass;
import models.Subject;
import models.User;
import play.data.validation.*;
import play.db.jpa.JPA;
import play.mvc.Controller;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static controllers.Secure.Security.connected;

public class Student extends Controller{
    public static void registryForm(){
        render();
    }

    public static void consultForm(){
        List<User> usersList = Consultation.find("select distinct user from Consultation").fetch(); //pobiera z bazy danych wszystkie konsultacje
        Map<User,List<Consultation>> userConsultationMap = new HashMap<User, List<Consultation>>();
        Map<Long,List<Consultation>> userConsultationMapIds = new HashMap<Long, List<Consultation>>();
        System.out.println("USERS LIST SIZE " + usersList.size());
        for(User user : usersList) {
            List<Consultation> consultationList = new ArrayList<Consultation>();
            consultationList = Consultation.find("byUser",user).fetch();
            userConsultationMap.put(user,consultationList);
            userConsultationMapIds.put(user.userId,consultationList);
        }
        Long userId = usersList.get(0).userId;
        System.out.println("USERS Map SIZE " + userConsultationMap.size());
        render(userConsultationMap,usersList,userConsultationMapIds,userId);
    }

    public static void catchupForm(){
        List<User> usersList = Subject.find("select distinct user from Subject").fetch();
        List<Subject> subjectList = Subject.findAll();
        Map<Long, User> userSubjectMap = new HashMap<Long,User>();
        Map<Subject, User> subjectUserHashMap = new HashMap<Subject,User>();
        Map<Long, Subject> subjectWithId = new HashMap<Long, Subject>();
        for(Subject subject : subjectList) {
            userSubjectMap.put(subject.subjectId,subject.user);
            subjectUserHashMap.put(subject,subject.user);
            subjectWithId.put(subject.subjectId,subject);
        }

        render(userSubjectMap,usersList,subjectList,subjectUserHashMap,subjectWithId);
    }

    public static void handleSubmit(
        @Required String username,
        @Required String surname,
        @Required String groupNumber,
        @Required @Email String email,
        @Required @MinSize(6) String password){

        // Handle errors
        if(validation.hasErrors()) {
            render("@registryForm");
        }

        User user = new User(username,surname,groupNumber,email,password);
        user.save();

        render(username, surname, groupNumber, email);
    }

    public static void sendConsultingMail(
            @Required String lecturer,
            @Required String choice,
            @Required String hour,
            @Required String classroom){
        User student = User.find("byEmail", connected()).<User>first();
        User lec = User.find("byEmail", lecturer).<User>first();
        Consultation consultation = Consultation.findById(Long.parseLong(choice));

        System.out.println("choice here "+ choice);
        System.out.println("Value here "+ consultation.classroom);

        MailClass.consultingMail(student, consultation);
        render("@sendMail");
    }

    public static void sendCatchingUpMail(
            @Required String lecturerAndDate,
            @Required String choice,
            @Required String day,
            @Required String hour,
            @Required String classroom,
            @Required String groupNumber){
        User student = User.find("byEmail", connected()).<User>first();
        Subject subject = Subject.findById(Long.parseLong(choice));

        MailClass.catchingUpMail(student, subject);
        render("@sendCatchingUp");
    }


}
