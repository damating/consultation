package controllers;


import models.User;


public class Security extends Secure.Security {

    static boolean authenticate(String email, String password) {
       return User.connect(email, password) != null;
    }

    static void onAuthenticated() {
        User user = User.find("byEmail", connected()).<User>first();
        if(user.type.equals("student")){
            Student.consultForm();
        }else{
            Admin.index();
        }
    }

    static boolean check(String profile) {
        User user = User.find("byEmail", connected()).<User>first();
        if(user.type.equals(profile)){
            return true;
        }else{
            return false;
        }
    }


}
