package models;

import net.sf.oval.constraint.Email;
import play.data.validation.Required;
import play.db.jpa.GenericModel;
import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class User extends GenericModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "USER_ID")
    public long userId;

    @Required
    @Column(name = "USERNAME")
    public String username;

    @Required
    @Column(name = "SURNAME")
    public String surname;

    @Required
    @Column(name = "GROUP_NUMBER")
    public String groupNumber;

    @Required
    @Email
    @Column(name = "EMAIL")
    public String email;

    @Required
    @Column(name = "PASSWORD")
    public String password;

    @Required
    @Column(name = "TYPE")
    public String type;

    public User(String username, String surname, String groupNumber, String email, String password) {
        this.username=username;
        this.surname=surname;
        this.groupNumber=groupNumber;
        this.email=email;
        this.password=password;
        type="student";
    }


    public Long getUserId() {
        return userId;
    }

    @Override
    public Object _key() {
        return getUserId();
    }

    public static User connect(String email, String password) {
        return find("byEmailAndPassword", email, password).first();
    }

    @Override
    public String toString() {
        return username + " " + surname;
    }
}
