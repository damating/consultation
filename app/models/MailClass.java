package models;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import play.libs.Mail;

import java.util.List;

public class MailClass extends Mail{

    public static void consultingMail(User userLogged, Consultation consultation){
        HtmlEmail email = new HtmlEmail();
        try {
            email.addTo(consultation.user.email);
            email.setFrom(userLogged.email, userLogged.username + " " + userLogged.surname);
            email.setSubject("Konsultacje");
            email.setHtmlMsg("<html>\n" +
                    "<body>\n" +
                    "\t<h2>Dzien dobry.</h2>\n" +
                    "\tJestem z grupy " + userLogged.groupNumber + ".<br/>\n" +
                    "\tChcialbym/Chcialabym umowic sie z Panem/Pania na konsultacje w dniu <b>" + consultation.day +"</b> miedzy <b>" + consultation.hour+ "</b> w <b>sali "+ consultation.classroom + "</b>.<br/>\n" +
                    "\tProsze o potwierdzenie spotkania.<br/>\n" +
                    "\t<br/>\n" +
                    "\tZ wyrazami szacunku,<br/>\n" +
                    "\t" + userLogged.username + " " + userLogged.surname + "\n" +
                    "</body>\n" +
                    "</html>");
            Mail.send(email);
        } catch (EmailException e) {
            e.printStackTrace();
        }
    }

    public static void catchingUpMail(User userLogged, Subject subject){
        HtmlEmail email = new HtmlEmail();
        try {
            email.addTo(subject.user.email);
            email.setFrom(userLogged.email, userLogged.username + " " + userLogged.surname);
            email.setSubject("Odrabianie");
            email.setHtmlMsg("<html>\n" +
                    "<body>\n" +
                    "\t<h2>Dzien dobry.</h2>\n" +
                    "\tJestem z grupy " + userLogged.groupNumber + ".<br/>\n" +
                    "\tChcialbym/Chcialabym odrobic u Pana/Pani zajecia z przedmiotu <b>" + subject.subjectName +"</b> w <b>" + subject.day +"</b> miedzy <b>" + subject.hour+ "</b> w <b>sali "+ subject.classroom + "</b>.<br/>\n" +
                    "\tProsze o potwierdzenie czy jest taka mozliwosc.<br/>\n" +
                    "\t<br/>\n" +
                    "\tZ wyrazami szacunku,<br/>\n" +
                    "\t" + userLogged.username + " " + userLogged.surname + "\n" +
                    "</body>\n" +
                    "</html>");
            Mail.send(email);
        } catch (EmailException e) {
            e.printStackTrace();
        }
    }


}
