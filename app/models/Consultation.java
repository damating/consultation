package models;

import play.data.validation.Required;
import play.db.jpa.GenericModel;
import play.db.jpa.Model;

import javax.persistence.*;

@Entity
public class Consultation extends GenericModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CONSULTATION_ID")
    public long consultationId;

    @Required
    @Column(name = "DAY")
    public String day;

    @Required
    @Column(name = "HOUR")
    public String hour;

    @Required
    @Column(name = "CLASSROOM")
    public String classroom;

    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = true)
    public User user;

    public Long getConsultationId() {
        return consultationId;
    }

    @Override
    public Object _key() {
        return getConsultationId();
    }

}
