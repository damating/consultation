package models;

import play.data.validation.Required;
import play.db.jpa.GenericModel;
import play.db.jpa.Model;

import javax.persistence.*;

@Entity
public class Subject extends GenericModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "SUBJECT_ID")
    public long subjectId;

    @Required
    @Column(name = "SUBJECT_NAME")
    public String subjectName;

    @Required
    @Column(name = "GROUP_NUMBER")
    public String groupNumber;

    @Required
    @Column(name = "DAY")
    public String day;

    @Required
    @Column(name = "HOUR")
    public String hour;

    @Required
    @Column(name = "CLASSROOM")
    public String classroom;

    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = true)
    public User user;

    public Long getSubjectId() {
        return subjectId;
    }

    @Override
    public Object _key() {
        return getSubjectId();
    }
}
